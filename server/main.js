
if (Meteor.isServer) {

  Meteor.startup(function () {
    // code to run on server at startup

	Meteor.publish("Snippets", function(){
		return snippets.find({});
	});
	Meteor.publish("users", function(){
    	return Meteor.users.find({},{limit:4,fields:{emails:0}});
	});


//permissions
snippets.allow({
  insert: function (userId,doc) {
    // the user must be logged in
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    // can only change your own documents
    return false;
  },
  remove: function (userId, doc) {
    // can only remove your own documents
    return doc.ownerID === userId;
  },
  fetch: ['ownerID']
});

//FIXME
Meteor.users.allow({
  insert: function (userId,doc) {
    // the user must be logged in
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    // can only change your own documents
    return doc.userId === userId;
  },
  fetch: ["_id"]
});



snippets.deny({
	insert: function(userId, doc) {
  		return doc.ownerID !== Meteor.userId();
},
  fetch: ['ownerID']
})
snippets.allow({
	update: function(userId, doc,fields,modifier) {
  		//can change score
  		return _.contains(fields, "score");
  }
})



//end startup
  });


	Meteor.methods({
		insertSnippet: function (_title,_about,_code) {
			if (_title && _code && _about){
				snippets.insert({title:_title,about:_about,code:_code,ownerID:Meteor.userId(),createdAt:new Date().getTime(),score:[Meteor.userId()]});
			}
		}
	});
  Meteor.methods({
  upScore: function (_user,_ownerID) {
      console.log(_ownerID)
      var user = Meteor.user();
      var snipScoreArray = snippets.findOne({_id:_user}).score;     
      if (_.include(snipScoreArray, user._id)){
        console.log("you already voted on this post");
      }else{
         snippets.update(_user, {$addToSet:{score:Meteor.userId()}});
        try{
          Meteor.users.update(_ownerID,{$inc:{"profile.score":1}});
        }catch(err){
           Meteor.users.update(_ownerID,{$set:{"profile.score":1}});
        }
      }
  }
//
});



//end server
}