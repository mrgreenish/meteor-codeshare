
snippets = new Meteor.Collection("Snippets");


if (Meteor.isClient) {

 Meteor.startup(function () {


// Style first ten lines, and forbid the cursor from entering them
    console.log("startup");// code to run on server at startup
    $('ul#leaderboard>li').tsort('span.score');
    Deps.autorun(function () {
      Meteor.subscribe("Snippets");
    });
    Deps.autorun(function () {
      Meteor.subscribe("users");
  });
});



  Accounts.ui.config({
    passwordSignupFields: 'USERNAME_AND_EMAIL'
  });


  Template.hello.greeting = function () {
    return "Welcome ";
  };


  Template.hello.loggedInUser = function(){
    if (Meteor.users.findOne({_id:Meteor.userId()}).profile){
      return Meteor.users.findOne({_id:Meteor.userId()}).profile.name;
    }else{
      Meteor.users.findOne({_id:Meteor.userId()}).username;
    }
        
  };

  Template.snippets.snippet = function(){
    return snippets.find({},{sort:{createdAt:-1},reactive:true});
 }

  Template.snippets.events({
    'click' : function () {
     Session.set("selected_player", this._id);
     Session.set("selected_user",this.ownerID);
      //snippets.update(this._id, {$inc:{score:5}});
    }
  });

  Template.codelist.owner = function (){
    console.log("OWNER " + this.ownerID + " " + Meteor.userId())
    if (this.ownerID === Meteor.userId()){
      return true;
    }else{
      return false;
    }
  }

  Template.codelist.events({
    'click .destroy-post' : function(){
      snippets.remove(this._id);
    }
  });

  Template.snippets.events({
    'click .scoreUP' : function () {
     var item = snippets.find({_id:Session.get("selected_player")}).fetch();
     var numberOfscores = item[0].score.length;
     for(var i = 0;i<numberOfscores; i++){
        if (item[0].score[i] === Meteor.userId()){
          Session.set("notAllowed", true);
        }
     }
    if (Session.get("notAllowed") === true) {
      //upScore();
      Meteor.call("upScore",Session.get("selected_player"),Session.get("selected_user"));
     }
    }
  });

  //function upScore(){
  //  snippets.update(Session.get("selected_player"), {$push:{score:Meteor.userId()}})
  //}

  Template.codeScore.score = function(){
    var sc =  snippets.findOne(this._id).score.length;
    return sc
  }

  Template.userList.allUsers = function(){
    var allUsers = Meteor.users.find({},{sort:{"profile.score":-1}});
    return allUsers;
  };


  Template.userItem.nameUser = function(){
    console.log(">>>>>>" +Meteor.users.find({_id:this._id}))
    var usr = Meteor.users.find({_id:this._id});
    return usr;
  } 




}


